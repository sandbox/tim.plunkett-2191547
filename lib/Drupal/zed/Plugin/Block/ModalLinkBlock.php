<?php

/**
 * @file
 * Contains \Drupal\zed\Plugin\Block\ModalLinkBlock.
 */

namespace Drupal\zed\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Component\Utility\Json;

/**
 * @Block(
 *   id = "zed_modal_link",
 *   admin_label = @Translation("Modal Link")
 * )
 */
class ModalLinkBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = array();
    $build['link'] = array(
      '#type' => 'link',
      '#title' => $this->t('Add new article'),
      '#route_name' => 'node.add',
      '#route_parameters' => array(
        'node_type' => 'article',
      ),
      '#attributes' => array(
        'class' => array('use-ajax'),
        'data-accepts' => 'application/vnd.drupal-modal',
        'data-dialog-options' => Json::encode(array(
          'width' => 'auto',
        )),
      ),
    );
    return $build;
  }

}
