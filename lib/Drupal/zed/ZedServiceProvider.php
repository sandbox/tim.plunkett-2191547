<?php

/**
 * @file
 * Contains \Drupal\zed\ZedServiceProvider.
 */

namespace Drupal\zed;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @todo.
 */
class ZedServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('config.storage.active')
      ->setClass('Drupal\Core\Config\CachedStorage')
      ->setArguments(array(new Reference('zed.config.storage'), new Reference('cache.config')));
  }

}
